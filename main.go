package main

import (
	"context"
	"fmt"

	"cloud.google.com/go/pubsub"
	"google.golang.org/api/option"
)

func callback(c context.Context, m *pubsub.Message) {
	fmt.Println(m.Data)
}

func main() {

	//initialse background
	ctx := context.Background()
	fmt.Println("Starting listner...")

	// crete client to connect to project
	proId := "gw-uat-mgmt-delhi"
	credPath := "/Users/ayushshrivastava/Desktop/project/go_pubsub_listner/credentials.json"
	client, err := pubsub.NewClient(ctx, proId, option.WithCredentialsFile(credPath))
	if err != nil {
		panic(err)
	}
	defer client.Close()

	// initiate connection to subscriptionn
	subID := "listner-golang-sub"
	sub := client.Subscription(subID)

	// starting listner as a subroutine
	err = sub.Receive(ctx, callback)
	if err != nil {
		panic(err)
	}

	fmt.Println("Ending listening...")
}
